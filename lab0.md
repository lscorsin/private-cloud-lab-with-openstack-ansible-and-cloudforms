## Lab 0: Check the environment

### Obtain lab GUID

1. Follow the directions [here](guid-grabber.md) in order to obtain your lab GUID. This GUID will be used to access your unique lab environment.
2. In your lab information webpage, take note of your assigned GUID. You will use this GUID to access your lab's systems. It will be embedded in your lab environment's host names.
3. From the lab environment information page, copy the hostnames of the following systems:
    - OpenStack (openstack-GUID.rhpds.opentlc.com)
    - CloudForms (cloudforms-GUID.rhpds.opentlc.com)
    - Ansible Tower (tower-GUID.rhpds.opentlc.com)


### Check all the Red Hat Products

You access the Red Hat Products you'll use in this lab in order to verify the services are ready to be used. If you have issues accessing any of the URLs, raise your hand and call any of the presenters.

**NOTE**: Login names and passwords will be provided in the following labs.

**IMPORTANT**: When accessing the services for the first time there will be warnings about the security certificates not being trusted. This is expected and you should accept the certificates.

#### Access **OpenStack Dashboard**.

To access the OpenStack Dashboard, use the URL provided to you (eg. https://openstack-GUID.rhpds.opentlc.com). There's no need to log into the service yet. The goal is to check if it is online.

You should see a screen like the following image:

![OpenStack Dashboard](images/openstack_horizon.png)

#### Access **CloudForms Administrative User Interface**.

To access CloudForms, the URL will be https://cloudforms-GUID.rhpds.opentlc.com, where GUID should be the one assigned to you. There's no need to log into the service yet. The goal is to check if it is online.

You should see a screen like the following image:

![cloudforms_admin_interface](images/cloudforms_admin_interface.png)

#### Access **Ansible Tower User Interface**.

To access Ansible Tower, the URL will be https://tower-GUID.rhpds.opentlc.com, where GUID should be the one assigned to you. There's no need to log into the service yet. The goal is to check if it is online.

You should see a screen like the following image:

![tower_interface](images/tower_interface.png)

[Table of Contents](README.md#table-of-contents) | [Next (Lab 1)](lab1.md)
