## Lab 5: Automate services delivery using Red Hat CloudForms and Red Hat Ansible Tower

In this lab you'll learn:
- How to integrate CloudForms and Ansible Tower to automate service delivery

### CloudForms Service Bundles Overview

Your apps have to live somewhere. If you're PXE booting and kickstarting bare-metal servers or VMs, or creating virtual or cloud instances from templates, Ansible and Red Hat Ansible Tower help streamline the process in conjunction with Red Hat CloudForms.

We are using Ansible Tower as an Automation Provider for CloudForms, and we want to successfully provision a Web Application (Ticketmonster Service Portal) within an OpenStack Instance. For this task, we need to create a new Service Bundle based on an OpenStack Instance Service Item to create a new Instance + Ansible Tower Job Template Service Item to deploy our Ticketmonster Web Application within it, and then access to **CloudForms Self-Service Portal** and select our new service to build our own Application.


### Access to CloudForms Cloud Management Portal

Access to ​**CloudFroms Cloud Management Portal​**, using the URL provided to you (eg. https://cloudforms-GUID.rhpds.opentlc.com​ )

Login    | Password
---------|---------
td-admin | r3dh4t1!

### Creating Service Catalog Items and Bundles

Through the use of catalogs, Red Hat CloudForms provides support for multi-tier service provisioning to deploy layered workloads across hybrid environments. You can create customized dialogs that will give consumers of the services the ability to input just a few parameters and provision the entire service. The following table lists the terminology associated with catalogs that you will use within the CloudForms user interface for service provisioning.

Now we need to create first two Service Catalog Items:

- **Create OpenStack Instance**. Based on Catalog Item Type: **OpenStack**.
- **Deploy Ticketmonster App on OSP Instance**. Based on Catalog Item Type: *AnsibleTower**.

Later, we'll be creating a Service Catalog Bundle using this Service Catalog Items.

#### Create an OpenStack Service Catalog Item

1.  On CloudForms Cloud Management Portal, navigate to the **Catalogs** section in the accordion, **Services --> Catalogs** and then **Catalog Items --> All Catalog Items --> OpenStack Catalog** and click on **Configuration --> Add a New Catalog Item**.

2. Select **OpenStack** from the drop-down list:

![service_new_openstack_item](images/service_new_openstack_item.png)

3. Fill out information as follows:
    - Basic Info tab
      - **Name/Description:** Create OSP Instance
      - **Display in Catalog:** ***Keep it unchecked***
      - **Catalog:** Test-Drive/OpenStack Catalog
      - **Dialog:** ***No Dialog***
      - **Provisioning Entry Point:** Leave the Default (/Service/Provisioning/StateMachines/ServiceProvision_Template/CatalogItemInitializaton)
      - **Reconfigure Entry Point:** Leave the default (empty)
      - **Retirement Entry Point:** Leave the default (/Service/Retirement/StateMachines/ServiceRetirement/Default)

    ![service_openstack_basic.png](images/service_openstack_basic.png)

    - Request Info tab
      - Catalog:
        - **Select Image Name:** RHEL-7
        - **Number of Instances:** 1
        - **Instace Name:** changeme

    ![service_openstack_catalog.png](images/service_openstack_catalog.png)

      - Environment:
        - **Cloud Tenant:** test-drive
        - **Availability Zones:** nova
        - **Cloud Network:** **_private_**
        - **Security Groups:** SSH_HTTP_From_Any
        - **Public IP Address:** NONE (we'll select one using a Service dialog later)

    ![service_openstack_environment.png](images/service_openstack_environment.png)

      - Properties:
        - **Instance Type:** m1.small
        - **Guest Access Key Pair:** test-drive

    ![service_openstack_properties.png](images/service_openstack_properties.png)

4. Click on **Add** to create the new OSP Service item.

#### Create an Ansible Tower Service Catalog Item

1.  On CloudForms Cloud Management Portal, navigate to the **Catalogs** section in the accordion, **Services --> Catalogs** and then **Catalog Items --> All Catalog Items --> OpenStack Catalog** and click on **Configuration --> Add a New Catalog Item**.

2. Select **AnsibleTower** from the drop-down list:

  ![service_new_ansibletower_item](images/service_new_ansibletower_item.png)

3. Fill out information as follows:

    - Basic Info tab
      - **Name/Description:** Deploy Ticketmonster App on OSP Instance
      - **Display in Catalog:** ***Keep it unchecked***
      - **Catalog:** Test-Drive/OpenStack Catalog
      - **Dialog:** ***No Dialog***
      - **Provider**: Ansible Tower 3 Automation Manager
      - **Ansible Tower Job Template:** Deploy Ticketmonster App on OSP Instances
      - **Provisioning Entry Point:** Change to ***/AutomationManagement/AnsibleTower/Service/Provisioning/StateMachines/Provision/provision_from_bundle*** (See NOTE below)
      - **Reconfigure Entry Point:** Leave the default (empty)
      - **Retirement Entry Point:** Leave the default (empty)

4. Click on **Add** to create the new OSP Service item.

  ![catalog_ansibletower](images/catalog_ansibletower.png)

  **NOTE:** To configure the **Provisioning Entry Point**, click on the text box with the current path, select the required entry point and click on **Apply**.

  ![ansibletower_entrypoint](images/ansibletower_entrypoint.png)

#### Create a Ticketmonster App Service Catalog Bundle

Now it's time to create a Service Catalog Bundle, using our Service Catalog Items created previously, so that we can create first an OpenStack Instance and then automate a configuration and an application deployment within it.

![cloudforms_service_bundle](images/cloudforms_service_bundle.png)

1. On CloudForms Cloud Management Portal, navigate to the **Catalogs** section in the accordion, **Services --> Catalogs** and then **Catalog Items --> All Catalog Items --> OpenStack Catalog** and click on **Configuration --> Add a New Catalog Bundle**.

1. Fill out information as follows:
    - Basic Info tab
      - **Name/Description:** Ticketmonster Service
      - **Display in Catalog:** ***Checked***
      - **Catalog:** Test-Drive/OpenStack Catalog
      - **Dialog:** ***Ticketmonster Service Dialog***
      - **Provisioning Entry Point:** Leave the default (/Service/Provisioning/StateMachines/ServiceProvision_Template/CatalogBundleInitializaton)
      - **Reconfigure Entry Point:** Leave the default (empty)
      - **Retirement Entry Point:** Leave the default (/Service/Retirement/StateMachines/ServiceRetirement/Default)
1. Include a service description on the Details tab
    - **Long Description:** My JBoss Service
1. On the Resources tab, choose **Create OSP Instance** from the **Add a Resource** dropdown menu.

    ![service_bundle_create_osp_instance](images/service_bundle_create_osp_instance.png)
1. Also on the Resources tab, choose **Deploy Ticketmonster App on OSP Instance** from the **Add a Resource** dropdown menu.

    ![service_bundle_deploy_ticketmonster](images/service_bundle_deploy_ticketmonster.png)
1. Now you need to adjust provision order of each resource and also the delay between them. Configure these settings according to the following instructions:
    - Create OSP Instance
      - **Action Order:** 1
      - **Provision Order:** 1
    - Deploy Ticketmonster App on OSP Instance
    - **Action Order:** 2
    - **Provision Order:** 2
    - **Delay (mins) Start:** 1
    ![service_bundle_resources](images/service_bundle_resources.png)
1. Click on **Add** to create the new Service Bundle.

When you finish these tasks, your Catalog will look like the following image.

![service_catalog](images/service_catalog.png)

---

### Self-service User Interface Overview

Red Hat CloudForms Self Service is a web-based graphical user interface for ordering and managing IT service requests. You can enable self-service tenant end users, who can easily access their services, track requests, and manage their accounts using the Self Service user interface (SSUI), which has widgets, dashboard controls and feedback. The Self Service user interface supports role-based access control (RBAC) of menus and features, similar to in the full administrative user interface.

### Ordering a new Service using CloudForms Self-service Portal

1. Access to CloudForms Self-Service Portal, using the URL provided to you, adding /self_service context-root
(eg. https://cloudforms-GUID.rhpds.opentlc.com/self_service)

Login    | Password
---------|---------
td-admin | r3dh4t1!

2. Click on **Service Catalog**
3. Click on **Ticketmonster Service**
4. Fill out required information as follows:

    - **Service Name:** My JBoss Service
    - **VM Name:** jboss-instance
    - **Flavor:** m1.small
    - **Floating IP:** 192.168.200.108

    ![ssui_ticketmonster_request](images/ssui_ticketmonster_request.png)
5. Click on **Add to Shopping Cart** button.
6. Click on your **Shopping Cart icon** at the upper right corner and click on **Order** to order your service.
Meanwhile, use the URL provided to you to be sure **Ticketmonster Web App** is not installed, using it with a web browser and adding :8080 port and context-root /ticket-monster as follows: http://floating108-GUID.rhpds.opentlc.com:8080/ticket-monster
7. Return to the **CloudForms Cloud Management Engine Portal** (https://cloudforms-GUID.rhpds.opentlc.com), go to *Services --> Requests* and click on **Reload** button until **Request State** is in **Finished** state.
8. Switch to **OpenStack Dashboard** to observe the deployment of the new OpenStack instance from a Service. Click on *Instances --> jboss-instance* to see the instance details.
9. Switch to **Ansible Tower Portal**. Click on *JOBS --> Deploy Ticketmonster App on OSP Instances* and take a look at all information. Note that the job will only show up when CloudForms triggers it, which will take a couple of minutes after you can see the instance in OpenStack.
10. Go to your URL provided but adding :8080 port and context-root /ticket-monster: http://floating108-GUID.rhpds.opentlc.com:8080/ticket-monster

    ![ticketmonster_deployed](images/ticketmonster_deployed.png)

Now you can access to your new Ticketmonster Web App using your link and it's time to play with it!

[Previous (Lab 4)](lab4.md) | [Table of Contents](README.md#table-of-contents)
