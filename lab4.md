## Lab 4: Configure virtual instance using Red Hat Ansibe Tower

In this lab you'll learn:
- How to create Job Templates in Ansible Tower
- How to run Job Templates to automate tasks

### Red Hat Ansible Tower Overview

Ansible seamlessly unites workflow orchestration with configuration management, provisioning, and application deployment in one easy-to-use and deploy platform.

Regardless of where you start with Ansible, you'll find our simple, powerful and agentless automation platform has the capabilities to solve your most challenging problems.

Centralizing configuration file management and deployment is a common use case for Ansible, and it's how many power users are first introduced to the Ansible automation platform. In that way, it's very easy to automate the configuration of your bare-metal servers, virtual machines and cloud instances.

When you define your application with Ansible, and manage the deployment with Ansible Tower, teams are able to effectively manage the entire application lifecycle from development to production.


### Access to Ansible Tower Portal

Access to ​**Ansible Tower Portal​**, using the URL provided to you (eg. https://tower-GUID.rhpds.opentlc.com​ )

Login    | Password
---------|---------
td-admin | r3dh4t1!

### Setting Up Credentials in Ansible Tower

Credentials are utilized by Tower for authentication when launching Jobs against machines, synchronizing with inventory sources, and importing project content from a version control system.

You can grant users and teams the ability to use these credentials, without actually exposing the credential to the user. If you have a user move to a different team or leave the organization, you don't have to re-key all of your systems just because that credential was available in Tower.

1. Navigate within the **Ansible Tower Portal** and go to **RESOURCES --> CREDENTIALS**. Click on ![](images/tower_add.png) at the right to add new Credentials:

1. Create the new Credentials as follows:
    - **Name:** Machine Credentials
    - **Description:** Machine Credentials for OSP Instances
    - **Organization:** Default
    - **Credential Type:** Machine
    - **Username:** cloud-user
    - **SSH Private Key:** Copy and paste the the <a href="https://gitlab.com/redhatsummitlabs/private-cloud-lab-with-openstack-ansible-and-cloudforms/raw/master/test-drive.pem" target="\_blank">test-drive.pem</a> private key
    - **Privilege Escalation Method:** sudo

1. Click on **SAVE** to confirm the changes.

  ![tower_credentials](images/tower_credentials.png)

### Creating Ansible Tower Job Templates

A job template is a definition and set of parameters for running an Ansible job. Job templates are useful to execute the same job many times. Job templates also encourage the reuse of Ansible playbook content and collaboration between teams. While the REST API allows for the execution of jobs directly, Tower requires that you first create a job template.

Navigate within the **Ansible Tower Portal** and click on **RESOURCES --> TEMPLATES**.

This menu opens a list of the job templates that are currently available. The Templates tab also enables the user to launch, schedule, modify, and remove a job template.

#### Job Template 1: Deploy Wordpress

1. Click on **TEMPLATES** and then click on the button ![](images/tower_add.png) available at the upper right side of the screen. Then, select **Job Template**.
2. Enter the following information:
    - **Name:** Deploy Wordpress App on OSP Instances
    - **Description:** Deploy Wordpress App on OSP Instances
    - **Job Type:** Run
    - **Invetory:** OpenStack Inventory
    - **Project:** OpenStack Project
    - **Playbook:** ***osp-tower-ansible-install-wordpress.yml***
    - **Credential:** Machine Credentials
    - **Limit:** ***wordpress-instance***
    - **Verbosity:** 0 (Normal)
    - **Options:** Enable Privilege Escalation: ***checked***

1. Click on **SAVE** to confirm the changes.


**NOTE:** In order to select a credential, click on ![](images/tower_search.png), select _Machine Credentials_ and finally click on _SELECT_.

![tower_select_credentials](images/tower_select_credentials.png)

This Job Template will look like the following image.

![tower_job_template_wordpress](images/tower_job_template_wordpress.png)

#### Job Template 2: Deploy Ticketmonster Application

**NOTE:** We'll be using this Job Template in the next lab only, when we'll deploy an
instance _and_ the Ticketmonster application using a Service Bundle in CloudForms. However, let's create the Job template here to practice a bit more.

1. Click on **TEMPLATES** and then click on the button ![](images/tower_add.png) available at the upper right side of the screen. Then, select **Job Template**.
2. Enter the following information:
    - **Name:** Deploy Ticketmonster App on OSP Instances
    - **Description:** Deploy Ticketmonster App on OSP Instances
    - **Job Type:** Run
    - **Invetory:** OpenStack Inventory
    - **Project:** OpenStack Project
    - **Playbook:** **_osp-tower-ansible-install-ticketmonster.yml_**
    - **Credential:** Machine Credentials
    - **Limit:** **_Keep it in blank_** and _Prompt on Launch_ **_checked_** (This is very important)
    - **Verbosity:** 0 (Normal)
    - **Options:** Enable Privilege Escalation: **_checked_**
1. Click on **SAVE** to confirm the changes.

This Job Template will look like the following image.

![tower_job_template_ticketmonster](images/tower_job_template_ticketmonster.png)

### Launching Ansible Tower Job Templates

Now it's time to deploy our new **Wordpress** App within **wordpress-instance** previously created via CloudForms.

1. First, use the URL provided to you to be sure **Wordpress** is not installed, using it with a web browser: http://floating106-GUID.rhpds.opentlc.com/wordpress

1. Then, go back to **Ansible Tower Portal**. Click on **TEMPLATES** and find the **"Deploy Wordpress App on OSP Instances"** Job Template, using the **SEARCH** textbox as follows:

  ![tower_search_job_template_wordpress](images/tower_search_job_template_wordpress.png)

1. Click on ![](images/tower_launch_job.png) to start a job using this template.

  ![tower_job_start](images/tower_job_start.png)  

This Action will launch the **"Deploy Wordpress App on OSP Instances"** Job Template, starting the Website installation. You will be redirected to a dynamic Job Output page:

![tower_job_output_wordpress](images/tower_job_output_wordpress.png)

You will see the Status of the Job, Started / Finished Time, Tasks, etc.

If your Job finished with Successful Status, go to your **Wordpress URL** (http://floating106-GUID.rhpds.opentlc.com/wordpress) again and make sure your Wordpress Server is **Up & Running**. Play a moment with your new Wordpress!

![tower_wordpress_screen](images/tower_wordpress_screen.png)

You can take a look at the Ansible Playbook we are using for this Job Template at https://github.com/vagnerfarias/openstack-ansible/blob/summit2019/osp-tower-ansible-install-wordpress.yml


[Previous (Lab 3)](lab3.md) | [Table of Contents](README.md#table-of-contents) | [Next (Lab 5)](lab5.md)
