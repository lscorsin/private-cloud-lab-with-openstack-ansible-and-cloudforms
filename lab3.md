## Lab 3: Provision virtual instance using Red Hat CloudForms

In this lab you'll learn:
- How the use of CloudForms to provision virtual instances in OpenStack

### Red Hat CloudForms Overview

Red Hat CloudForms is an infrastructure management platform that allows IT
departments to control users self-service abilities to provision, manage, and ensure
compliance across virtual machines and private clouds.

Provisioning refers to the capacity an infrastructure has to deliver a resource and
manage its life cycle. Provisionable resources can include virtual machines, Instances,
storage space, or any resource a given infrastructure can manage. The web interface on
a ​*CloudForms*​ appliance provides an easy way for end users and administrators to
provision new virtual machines and Instances. Virtual machines can be provisioned
from virtual machine templates, PXE boot, or ISO images. Instances can be provisioned
from images.

### Cleaning Up Our Environment

First, we need to clean up our environment so that we can reuse resources such as
CPU, Memory, vDisks and of course ours Floating IPs.

1. Access to ​**CloudForms Cloud Management Portal​**, using the URL provided to you (eg. https://cloudforms-GUID.rhpds.opentlc.com​ )

Login    | Password
---------|---------
td-admin | r3dh4t1!

2. Go to: **Compute​ --> Clouds --> Instances** and select ​**instance2**:

  ![cloudforms_select_instance2](images/cloudforms_select_instance2.png)

3. Click on **Power --> Delete**:

  ![cloudforms_delete_instance2](images/cloudforms_delete_instance2.png)

4. Wait some seconds or check in OpenStack Dashboard that the instance was deleted. Go to **Compute --> Clouds --> Providers** and then click on **Red Hat OpenStack Platform​ provider** to enter in the summary view:

  ![cloudforms_openstack_summary](images/cloudforms_openstack_summary_view.png)
  NOTE: if you don't see this Summary View, click on the button on the upper right corner, as shown in the image.

5. After that, click on **Configuration --> Refresh Relationships and Power States​** to update all information about our Red Hat OpenStack Provider:

  ![cloudforms_refresh_openstack_provider](images/cloudforms_refresh_openstack_provider.png)

6. Finally, click on Refresh button ![](images/cloudforms_refresh_button.png) on CloudForms user interface until you can see **"Last Refresh: Success - Less Than a Minute Ago"**

  ![cloudforms_openstack_summary](images/cloudforms_openstack_summary.png)

**NOTE:** This step to refresh the provider relationships is needed because some notifications from OpenStack to CloudForms are disabled for this lab environment, which means they only are only happening in determined intervals, but we don't want to wait.

### Creating a New Instance Using CloudForms

1. Access to **CloudForms Cloud Management Portal**, using the URL provided to you (eg. https://cloudforms-GUID.rhpds.opentlc.com​).

Login    | Password
---------|---------
td-admin | r3dh4t1!

2. Go to: ​**Compute​ --> Clouds --> Instances** and click on​ **Lifecycle --> +Provision
Instances**. Select the Template ​**RHEL-7** as Image. Click on **Continue** and fill out the information as follows:
    - Request tab
      - **Email:**  your.email@yourdomain.com
      - **First/Last Name:** your name (if you want. Not necessary)
    - Purpose tab
      - **Purpose:** Select *Environment --> Test*
    - Catalog tab
      - **Number of instances**: 1
      - **Instance name:** wordpress-instance
    - Environment tab
      - **Cloud Tenant:** test-drive
      - **Availability Zones:** nova
      - **Cloud Network:** private
      - **Security Groups:** SSH_HTTP_From_Any
      - **Pubilc IP Address:** 192.168.200.106
    - Properties tab
      - **Instance Type:** m1.small
      - **Guest Access Key Pair:** test-drive
    - Schedule tab
      - **Time until Retirement:** 1 Month
      - **Retirement Warning:** 1 Week

3. Click on **Submit** to create the new instance.
4. Switch to **OpenStack Dashboard** using URL provided to you (eg. https://openstack-GUID.rhpds.opentlc.com). Observe the deployment of the new OpenStack instance (Project --> Compute --> Instances).
5. Click on ​**Instances​ ​--> wordpress-instance**​ to see the instance details.
6. Return to ​**CloudForms Management Portal​**. Go to ​**Services --> Requests**. Click on **Reload**​ button, until the ​**Last Message​ ​"CF_Worker1: VM [wordpress-instance]
Provisioned Successfully​"** appears and ​**Request State**​ is in ​**Finished​** state:

  ![cloudforms_wordpress-instance_provisioned](images/cloudforms_wordpress-instance_provisioned.png)

  ![cloudforms_wordpress-instance_request_finished](images/cloudforms_wordpress-instance_request_finished.png)


[Previous (Lab 2)](lab2.md) | [Table of Contents](README.md#table-of-contents) | [Next (Lab 4)](lab4.md)
