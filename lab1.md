## Lab 1: Provision virtual instances on Red Hat OpenStack Platform

This first lab aims to provide an overview of the OpenStack Dashboard (Horizon) and you'll learn to:

- Create virtual machines (commonly called virtual instances in cloud environments) connected to an existing network.
- Access the console of a virtual instance.
- Use floating IP addresses to allow inbound access to virtual instances

### OpenStack Dashboard Overview
To access the OpenStack Dashboard, use the URL provided to you (eg. https://openstack-GUID.rhpds.opentlc.com).

![OpenStack Dashboard](images/openstack_horizon.png)

Use the following credentials to login:

Login   | Password
--------|---------
td-user | r3dh4t1!

This user will allow you to access a project called “test-drive” with a “_member_” role. This essentially means that this user is not an OpenStack administrator and is unable to manage user permissions, create new projects or manage quotas, among other administrative tasks.

Inside the "test-drive" project, however, this user can manage virtual instances, networks, routers, images, volumes.

First thing you may do is to explore the dashboard. In the top left section of the screen you have two tabs: Project and Identity.

![project_identity](images/project_identity.png)

As you're logged as a "\_member\_", the Identity tab will only show information about your user.  OpenStack administrators can manage users and projects.

In the Project tab you can access several other sections that will allow you to execute several tasks in the environment, as summarized in the following table.

Section       | Description
--------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Compute       | Provides an overview of used and available resources, considering user quota. It's also the section where you'll be able to create new instances, volumes,  images or key pairs as well as manage existing ones.
Network       | Provides a network topology diagram and allows you to manage networks, routers, security groups and floating IPs.
Orchestration | Provides an interface to the Orchestration service (Heat), which as the name implies allows orchestrating the creation/association of OpenStack resources to deploy applications based on a template.

At the top right corner of the screen you would be able to switch between projects if there were other projects you had access to, which is not the case at this moment. Also, clicking on your *username* you have the option to change some User Interface settings or your password. It’s also through this menu that you may *logout* from the dashboard.

### Create a new instance

To create a new instance go to *Project --> Compute --> Instances* and click on the “Launch Instance” button as shown in the diagram below.

![launch_instance_button](images/launch_instance_button.png)

The Launch Instance window will be opened and you need to provide at least the information marked with a “\*”. For this lab, you should use the following information:

- Details tab
  - **Instance name:** instance1
  - **Availability Zone:** nova
  - **Count:** 1
- Source tab
  - **Select Boot Source:** Image
  - **Create New Volume:** No
  - **Image:** RHEL-7
- Flavor tab
  - **Flavor:** m1.small
- Networks tab
  - **Network:** private

After the instance creation finishes you’ll be able to see it in the listing at *Project --> Compute --> Instances*. At this point, you can use the *Actions* menu and access the instance console.

![actions_menu_console](images/actions_menu_console.png)

To log into this instance, use:

Login | Password
------|---------
root  | r3dh4t1!

Use *ping* to test connectivity to the virtual router (10.10.10.1) and to an Internet address (eg. 8.8.8.8).

`[root@instance1 ~]# ping -c3 10.10.10.1`

`[root@instance1 ~]# ping -c3 8.8.8.8`

![instance1_ping_test](images/instance1_ping_test.png)

You can check the network topology at *Project --> Network--> Network Topology*.

![network_topology_instance1](images/network_topology_instance1.png)

### Create a second instance

In order to test connectivity between two instances, launch a new one (*Project → Compute --> Instances --> Launch Instance*), using the following information (note the different image):

- Details tab
  - **Instance name:** instance2
  - **Availability Zone:** nova
  - **Count:** 1
- Source tab
  - **Select Boot Source:** Image
  - **Create New Volume:** No
  - **Image:** ***webserver***
- Flavor tab
  - **Flavor:** m1.small
- Networks tab
  - **Network:** private
- Security Groups tab
  - **Allocated:** default, SSH_HTTP_From_Any

From the Instances list (*Project --> Compute --> Instances*), take note of the IP address of instance1. After that, connect to instance2 console, log into it and ping the IP address of instance1 and the router (10.10.10.1).

### Floating IP Overview

Typically in OpenStack environments the virtual instances will be connected to private networks that are known only withing the OpenStack cloud. In order to allow inbound access to a virtual instance connected to a virtual private network it's required to associate a floating IP to it. At this time, it’s important to differentiate Fixed IPs and Floating IPs.

Address Type | Description
-------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Fixed IP     | When an instance is created it receives at least one IP address, which is known as the fixed IP and is provided by DHCP according to the network to which the virtual NIC is connected to. The number of fixed IPs the instance will have depends on the number of virtual NICs configured. This IP address is visible from within the instance (eg. you can see it running "ip address show") and is typically part of a virtual private network.
Floating IP  | Floating IP addresses are used to allow external inbound access to instances connected to virtual private networks. The floating IPs are created on an external network and when it’s attached to a virtual instance,  neutron will configure a Destination NAT (DNAT) from the external IP address to the fixed IP address used by the instance.  Virtual instances don’t have any information about floating IP addresses and the address translation will be handled by neutron L3 agent (in the case of lab environment).

### Associate Floating IP to Virtual Instance

1. Logged as *td-user*, go to Projects --> Compute --> Instances.
2. In **instance2** Actions menu, select "Associate Floating IP"

    ![associate_floating_ip](images/associate_floating_ip.png)

3. In the next dialog, select the IP addresses **192.168.200.108** and click on "Associate".

    ![manage_floating_ip_associations](images/manage_floating_ip_associations.png)
    “Port to be associated” sets to which IP address the DNAT configuration should be done. In this case, the instance has only one port/IP address.


> **NOTE:** In this lab environent, only IP addresses 192.168.200.106 and 192.168.200.108  will be externally accessible.

When you finish this procedure, you’ll be able to see the floating IP associated to the instance, in addition to its fixed IP (Project --> Compute --> Instances):

![instance_with_floating_ip](images/instance_with_floating_ip.png)

### Test Access to Virtual Instance

In this environment, each of the two pre-configured IP addresses is associated to a resolvable hostname and corresponding DNAT configuration to the cloud provider in use. Using other floating IP addresses won't work as these are the only ones with mapping configured.

OpenStack Floating IP Address | hostname
------------------------------|-----------------------------------
192.168.200.106               | floating106-GUID.rhpds.opentlc.com
192.168.200.108               | floating108-GUID.rhpds.opentlc.com

If you created **instance2** with the "webserver" image as described before, you may validate that the floating IP is working by accessing the following URL from your browser: http://floating108-GUID.rhpds.opentlc.com. You should get to a web page like the following:

![floating108_webpage](images/floating108-webpage.png)

[Previous (Lab 0)](lab0.md) | [Table of Contents](README.md#table-of-contents) | [Next (Lab 2)](lab2.md)
